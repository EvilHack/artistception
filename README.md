 
# Artistception
This game is a 2D plataformer  created with Unity 2019 where the player enters to a different painting each level.
![Title](https://i.ibb.co/5rv8Q6z/level1.png)


## Features
The main mechanics are:
* 2 different graphic styles
* Educational dialogues
* 1 painting → 1 level
* Cinematics
* 2 bosses
* 2 levels
* Sounds
* Weapons
* Menu
* Game in Spanish and English
    
## Gameplay

   [![Gameplay]( )](tbd)

## Credits
* Programming

   * Joan Cortés(@EvilHack)
   * Roger Lario(@roger_lario)
   * Joan Lecha(@joanlecha)
* Art
   * Joan Lecha(@joanlecha)
   * Roger Lario(@roger_lario)
* Music  
   * Roger Lario friend
   * Copyleft Music 

