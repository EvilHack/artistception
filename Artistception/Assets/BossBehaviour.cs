﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour : MonoBehaviour
{
    private BossActionType eCurState = BossActionType.Idle;
    private bool dirRight = true;
    bool turning = false;
    bool jump = false;
    public float speed = 2.0f;
    public float jumpForce = 700f;
    int clickCount;
    bool facingRight = true;
    public Transform laser;
    public PlayerBehaviour player;
    public Transform subEnemy;
  //  public bool noSubEnemy = true;
    public int maxSubEnemy =1;
    public static int currentNumberOfSubEnemy=0;
  //  private EnemyBehaviour enemy;
    public Transform wallL;
    public Transform wallR;
    public Transform wallD;
    public Transform wallU;
    public Rigidbody2D rb;
    private GameManager GameController;
    int shootingLaser = 0;
    public AudioClip hitSound;
    public Transform explosion;
    public Animator animator;
    public SoundManager soundManager;
    public int health;
    void Start()
    {
        soundManager = FindObjectOfType<SoundManager>();
        animator = GetComponent<Animator>();
        player = FindObjectOfType<PlayerBehaviour>();
     //   enemy = GetComponent<EnemyBehaviour>();

        rb = GetComponent<Rigidbody2D>();
    }

    public enum BossActionType
    {
        Idle,
        Moving,
        Attacking,
        Die,
        Phase2Trigger
    }

    bool onWallLeft=false;
   private bool recentTeleport = false;

    IEnumerator WaitForTeleport(){
         yield return  new WaitForSeconds(0.3f);
         recentTeleport = false;

    }
 
     
 
    public  void KillBoss()
    {
        eCurState = BossActionType.Die;
        //Aqui Activare el portal
        Destroy(this.gameObject,animator.GetCurrentAnimatorStateInfo(0).length);
    
        
        if (explosion)
        {

            Transform t = (Transform)Instantiate(explosion, this.transform.position, this.transform.rotation);

            GameObject exploder = t.gameObject;

            Destroy(exploder, 2.0f);
        }
    }
    
    IEnumerator MoveLeft()
    {
        Debug.Log("Se mueve a la izquierda con rb");
        yield return new WaitForSeconds(3.0f);
        dirRight = false;
        if (dirRight == false)
            rb.AddForce(-Vector2.right * speed * Time.deltaTime , ForceMode2D.Impulse);
        turning = false;

        StartCoroutine("MoveRight");
       eCurState = BossActionType.Attacking;
    }

    IEnumerator MoveRight()
    {

        Debug.Log("Se mueve a la derecha");

        yield return new WaitForSeconds(3.0f);
        dirRight = true;
        if (dirRight)
            rb.AddForce(Vector2.right * speed * Time.deltaTime, ForceMode2D.Impulse);
        turning = true;

        StartCoroutine("MoveLeft");
  //      eCurState = BossActionType.Attacking;

    }

    void Update()
    {
        if (!PauseMenuBehaviour.isPaused)
        {
            switch (eCurState)
            {
                case BossActionType.Idle:
                    
                    // 
                      StartCoroutine("MoveLeft");


                   
                    break;

                case BossActionType.Moving:
                    
                    Debug.Log("Im moving");
                 
                    break;
       

                case BossActionType.Attacking:
                    //HandleAttackingState();
                    StartCoroutine("Attack");

                    break;
            }
        }
    }
    public IEnumerator LaserAtack() {
        int LeftOrRight = UnityEngine.Random.Range(0,1);
        if (LeftOrRight==0) {
            Debug.Log("Shooting laser");


                MoveLeft();
                ShootLaser();
                shootingLaser++;

        }
        else if(LeftOrRight==1){
            Debug.Log("Shooting laser");
            MoveRight();
            ShootLaser();
            shootingLaser++;

        }
        StartCoroutine("timeBetweenAttacks");
        yield return  new WaitForSeconds(3f);
    }
    public IEnumerator Attack() {

        Debug.Log("Atacking");
        int option = UnityEngine.Random.Range(0, 7);
      ///  Debug.Log("Probabilidad de ataque " + option);

        switch (option)
        {
            case 0: break;
            case 1:
                StartCoroutine("LaserAtack");

                break;
            case 2:
                Vector2 subenemypos = this.transform.position;
                Debug.Log("Creating enemy");
                subenemypos.x = this.transform.position.x;
                subenemypos.y = this.transform.position.y;
                if (currentNumberOfSubEnemy<maxSubEnemy)
                {
                    Debug.Log("A trabajar");
                    Instantiate(subEnemy, subenemypos, this.transform.rotation);
                    currentNumberOfSubEnemy++;
                    StartCoroutine("timeBetweenAttacks");
                }
                break;
            case 3:


                break;
            case 4:

                MoveLeft();
                break;
           case 5:
                MoveRight();
                break;

        }
        StartCoroutine("timeBetweenAttacks");
           new WaitForSeconds(3000f);
        eCurState = BossActionType.Idle;

         Debug.Log("Enemt recharged");
        yield return new WaitForSeconds(30f);
    }

    public void ShootLaser()
    {

    //    audioSource.PlayOneShot(shootSound);

        Vector2 laserPos = this.transform.position; //la posición de la nave

        float rotationAngle = this.transform.localEulerAngles.z -90; //grados

        laserPos.x += Mathf.Cos(rotationAngle * Mathf.Deg2Rad) ;
        laserPos.y += Mathf.Sin(rotationAngle * Mathf.Deg2Rad) ;
        Instantiate(laser, laserPos, this.transform.rotation);
    }
IEnumerator timeBetweenAttacks() {
        StopCoroutine(Attack());

        yield return new  WaitForSeconds(5f);
      //  Debug.Log(shootingLaser);
        //noSubEnemy = true;

        shootingLaser = 0;

    }
        void FixedUpdate(){

      
    }
    private IEnumerator OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("AttackProp")) {
            Debug.Log("the boss hurts");
            //   health -= LanceBehaviour.damage;
           // Destroy(collision.gameObject);
            GetComponent<AudioSource>().PlayOneShot(hitSound);

            if (health <= 0)
            {
                KillBoss();
            }

        }
        if (collision.gameObject.tag.Equals("Player"))
        {
            soundManager.PlaySE(hitSound,1);

        }
            yield return  new WaitForEndOfFrame();
    }
   
}
