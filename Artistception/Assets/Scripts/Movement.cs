﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    float velocidadX;
    public  float fuerzaSalto = 5f;
    Rigidbody2D rb;
    int saltosHechos;
    public int limiteSaltos = 2;
    public bool saltar;
    bool canMove;
    public Transform refPie;
    public AudioClip deathSound;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
       
        rb = GetComponent<Rigidbody2D>();
        saltosHechos = 0;
        animator = GetComponent<Animator>();
       // animator.keepAnimatorControllerStateOnDisable = false;
        animator.SetBool("morir", false);
        animator.Play("Quieto");
        saltar = animator.GetBool("saltar");
        canMove = true;
    }

    public Vector3 localScale= new Vector3(0.3f, 0.3f, 1);
    public float speed=3f;
    // Update is called once per frame
    void Update(){
        saltar = !Physics2D.OverlapCircle(refPie.position, 1f, 1 << 8);
        animator.SetBool("saltar", saltar);
        velocidadX = 0f;
        if (Input.GetKey(KeyCode.D) &&canMove)
        {
            transform.localScale = localScale;
            animator.SetBool("correr", true);
            velocidadX = speed;
        }
        if (Input.GetKey(KeyCode.A) && canMove)
        {
            transform.localScale = new Vector3(-localScale.x,localScale.y,localScale.z);
            animator.SetBool("correr", true);
            velocidadX = -speed;
        }

        transform.Translate(velocidadX * Time.deltaTime, 0f, 0f);

        if (velocidadX == 0f)
        {
            animator.SetBool("correr", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (saltosHechos < limiteSaltos)
            {
                rb.AddForce(new Vector2(0f, fuerzaSalto), ForceMode2D.Impulse);
                saltosHechos++;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
     
        if (collision.gameObject.layer==8)
        {
            rb.velocity = new Vector3(0f, 0f, 0f);
            transform.parent = collision.gameObject.transform.parent;
            saltosHechos = 0;
        }
        if (collision.collider.tag == "Dead")
        {

            Debug.Log("eSTO DU¡¡FUNCIONA 3");
            FindObjectOfType<SoundManager>().PlaySE(deathSound,1);
            animator.SetBool("morir", true);
            saltar = false;
            animator.SetBool("saltar",saltar);
            animator.SetBool("quieto", true);
            canMove = false;
            saltosHechos = 2;
        }
    }
}
