﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollowPlayer : MonoBehaviour
{

    public float speed;
    public float lineOfSite;
    public float shootingRange;
    public float fireRate = 1f;
    private float nextFireTime;
    public GameObject bullet;
    public GameObject bulletParent;
    private Transform player;
    Vector3 startPosition;
    public bool toRight;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = this.transform.position;
        InitializeFollow();
    }

    public Vector3 localScale = new Vector3(0.2f, 0.2f, 1);

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            float distanceFromPlayer = Vector2.Distance(player.position, transform.position);
            if (distanceFromPlayer < lineOfSite && distanceFromPlayer > shootingRange)
            {
                transform.position = Vector2.MoveTowards(this.transform.position, player.position, speed * Time.deltaTime);
            }
            else if (distanceFromPlayer <= shootingRange && nextFireTime < Time.time)
            {
                Instantiate(bullet, bulletParent.transform.position, Quaternion.identity);
                nextFireTime = Time.time + fireRate;
            }
            if (transform.position.x < player.position.x)
            {
                transform.localScale = new Vector3(-localScale.x, localScale.y, localScale.z);
                toRight = false;
            }
            else
            {
                transform.localScale = new Vector3(localScale.x, localScale.y, localScale.z);
                toRight = true;
            }

        }
        else if (GetComponent<Animator>() != null)
        {

            if (!GetComponent<Animator>().GetBool("IsDead")) InitializeFollow();

        }
        else { InitializeFollow(); }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSite);
        Gizmos.DrawWireSphere(transform.position, shootingRange);
    }
    public void InitializeFollow()
    {
        this.transform.position = startPosition;
        player = GameObject.FindGameObjectWithTag("Player").transform;

    }
}
