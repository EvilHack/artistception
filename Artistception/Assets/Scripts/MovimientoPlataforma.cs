﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPlataforma : MonoBehaviour
{

    public Transform target;
    public float speed;
    
    private Vector3 start, end;
    public Vector3 localScale = new Vector3(1f, 1f, 1);

    // Start is called before the first frame update
    void Start()
    {
        if (target != null)
        {
            target.parent = null;
            start = transform.position;
            end = target.position;
        }
    }

    private void FixedUpdate()
    {
        if (target != null)
        {
            float fixedSpeed = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.position, fixedSpeed);
        }

        if (transform.position == target.position)
        {
           
            if (target.position == start)
            {
                target.position = end;
                //transform.GetChild(0).transform.localScale = new Vector3(localScale.x, localScale.y, localScale.z);
         
                //                   GetComponent<SpriteRenderer>().flipX = false;
                //         GetComponent<PolygonCollider2D>(). = false;

            }
            else if (target.position == end)
            {

                target.position = start;
                //transform.GetChild(0).transform.localScale = new Vector3(-localScale.x, localScale.y, localScale.z);
               
                // this.transform.localScale = new Vector3(-localScale.x, localScale.y, localScale.z);

                //  GetComponent<SpriteRenderer>().flipX = true;
            }
            
        }
    }
}
