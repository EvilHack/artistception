﻿using Cinemachine;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    
    public static GameManager _instance;
    public int level ;
    public int levelToLoad;
    public PlayerBehaviour player;
    public SoundManager sm;
    public AudioClip[] levelsBGM = new AudioClip[4];
    public Transform spawnPoint;
    
    private void Awake()
    {
        if (GameManager._instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);

        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        sm = FindObjectOfType<SoundManager>();
        ChangeLevel(level);
 
      
    }
    public void OnLevelWasLoaded(int level)
    {
        level = SceneManager.GetActiveScene().buildIndex;
        sm = FindObjectOfType<SoundManager>();
        Debug.Log(level);
        sm.PlayBGM(levelsBGM[level]);
      

        switch (level) {
            case 0:
                GameObject.Find("Start Game").GetComponent<Button>().onClick.AddListener(() => {
                    ChangeLevel(1);
                });

                break;
            case 1:
                levelToLoad = 2;
                spawnPoint = GameObject.FindGameObjectWithTag("Respawn").transform;

                transform.position = spawnPoint.position;
                player = FindObjectOfType<PlayerBehaviour>();
                break;
            case 2:
                FindObjectOfType<Button>().GetComponent<Button>().onClick.AddListener(() => {
                    levelToLoad = 3;
                    SceneManager.LoadScene(4);
                });
                break;
            case 3:

                player = FindObjectOfType<PlayerBehaviour>();
                spawnPoint = GameObject.FindGameObjectWithTag("Respawn").transform;

                transform.position = spawnPoint.position;
                break;
            case 4:
               

                break;

        }



        
    }

    // Update is called once per frame
    void Update()
    {
     
    }
    public void ChangeLevel(int newLevel)
    {
        level = newLevel;
        levelToLoad = newLevel ;
        SceneManager.LoadScene(4);
    }
    public void IncreaseLevel()
    {
         
        ChangeLevel(level);
    }

     public void KillPlayer()
    {

        player = Instantiate(player);
        player.transform.position = this.transform.position;
        player.GetComponent<Animator>().Play("Salto");
        FindObjectOfType<CinemachineVirtualCamera>().Follow = player.gameObject.transform;
        
        //   player.transform.position = GetLastCheckPoint();
    }

    private Vector3 GetLastCheckPoint()
    {
        throw new NotImplementedException();
    }
}
