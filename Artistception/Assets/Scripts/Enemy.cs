﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Animator animator;

    public int maxHealth = 100;
    int currentHealth;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        //if (GetComponent<EnemyFollowPlayer>().toRight)
        //{

            //GetComponent<Rigidbody2D>().AddForce(Vector2.right);
        //}
        //else
        //{

            //GetComponent<Rigidbody2D>().AddForce(Vector2.left);
        //}


        // Hacer animacion de daño
        animator.SetTrigger("Hurt");

        if (currentHealth <= 0)
        {

            Die();
        }
    }

    void Die()
    {
        Debug.Log("Enemy died!");

        // Animacion morir
        animator.SetBool("IsDead", true);

        // Deshabilitar al enemigo
        GetComponent<Collider2D>().enabled = false;
        GetComponent<EnemyFollowPlayer>().enabled = false;
        this.enabled = false;
        Destroy(this.gameObject, 5);
    }
}
