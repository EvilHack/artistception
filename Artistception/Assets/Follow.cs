﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform target;
    public Vector2 distance;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            this.transform.position =  new Vector3( target.transform.position.x  + distance.x, target.transform.position.y+distance.y, this.transform.position.z);

        }
        else {
            DefaultInit();
        }
    }
    private void DefaultInit() {
        if (target == null) target = FindObjectOfType<PlayerBehaviour>().gameObject.transform;
    }
}
